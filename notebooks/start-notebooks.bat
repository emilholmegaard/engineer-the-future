@echo off
set LOCAL_PORT=
set startPort=16384

:EPHEMERAL_PORT
netstat -o -n -a | find "LISTENING" | find ":%startPort% " > NUL
if "%ERRORLEVEL%" equ "0" (
  set /a startPort +=1
  GOTO :EPHEMERAL_PORT
) ELSE (
  set LOCAL_PORT=%startPort%
  GOTO :FOUNDPORT
)

#Find empty port
GOTO:EPHEMERAL_PORT
:FOUNDPORT

#Get location of this script, for later copy of notebooks to jupyter
set NOTEBOOKS_DIR=%~dp0

#Start jupyter and get the name of the container
set CONTAINER_NAME=$(docker run -p $LOCAL_PORT:8888 -d -v $NOTEBOOKS_DIR:/home/jovyan jupyter/scipy-notebook)

#Sleep to make sure the log is written
sleep 3

#Get token for jupyter
#!!TODO - not working!!
set T=token=$(docker logs $CONTAINER_NAME 2>&1 | grep -o "token=[a-z0-9]*"| sed -n 1p | cut -c 7-)

#Construct URL
set URL="http://localhost:"$LOCAL_PORT"?"$T
#Open Jupyter
open $URL
